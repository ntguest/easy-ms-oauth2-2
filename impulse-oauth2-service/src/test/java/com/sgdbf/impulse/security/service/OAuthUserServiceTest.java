package com.sgdbf.impulse.security.service;

import com.google.common.collect.Lists;
import com.sgdbf.impulse.common.test.AbstractBaseTest;
import com.sgdbf.impulse.security.config.RealmContext;
import com.sgdbf.impulse.security.domain.OAuthUser;
import com.sgdbf.impulse.security.repository.OAuthUserRepository;
import com.sgdbf.impulse.security.utils.TokenHelper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;

import static com.sgdbf.impulse.security.utils.Constants.AROBASE;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author yfo.
 */
@RunWith(SpringRunner.class)
@SpringBootTest("oAuthUserService")
public class OAuthUserServiceTest extends AbstractBaseTest {

    @Inject
    private OAuthUserService oAuthUserService;
    @Inject
    private OAuthUserRepository oAuthUserRepository;

    @Before
    public void setUp() {
        super.setUp();
        RealmContext.setCurrentRealm("default");
    }

    @Test
    public void should_build_user_information_from_authentication() {
        OAuth2Authentication auth2Authentication = new OAuth2Authentication(TokenHelper.getOAuth2Request(), getUserAuthentication());
        Map<String, Object> userInformation = oAuthUserService.buildUserInformation("default", auth2Authentication);
        assertThat(userInformation.size()).isEqualTo(9);
        assertThat(userInformation.get("sub")).isEqualTo("1003@default");
        assertThat(userInformation.get("accountId")).isEqualTo("1003");
        assertThat(userInformation.get("login")).isEqualTo("user_account");
        assertThat(userInformation.get("firstName")).isNull();
        assertThat(userInformation.get("lastName")).isNull();
        assertThat(userInformation.get("impersonator")).isEqualTo(false);
        assertThat(userInformation.get("sgId")).isNull();
        assertThat((List<String>) userInformation.get("roles")).containsExactly("ACCOUNT");
        assertThat((List<String>) userInformation.get("sites")).containsExactly("default");
    }

    @Test
    public void should_not_build_user_information_when_authentication_is_null() {
        OAuth2Authentication auth2Authentication = new OAuth2Authentication(TokenHelper.getOAuth2Request(), null);
        Map<String, Object> userInformation = oAuthUserService.buildUserInformation("default", auth2Authentication);
        assertThat(userInformation).isEmpty();
    }

    @Test
    public void should_delete_oAuth_user_with_id() {
        oAuthUserService.delete("default", Lists.newArrayList("VI_4569"));
        OAuthUser oAuthUser = oAuthUserRepository.findOne("VI_4569@default");
        assertThat(oAuthUser).isNull();
    }

    private UsernamePasswordAuthenticationToken getUserAuthentication() {
        List<GrantedAuthority> authorityList = AuthorityUtils.createAuthorityList("ACCOUNT");
        ImpulseUserDetails user = new ImpulseUserDetails("1003", authorityList, Lists.newArrayList("default"));
        user.setUserId("1003" + AROBASE + "default");
        user.setAccountId("1003");
        user.setLogin("user_account");
        user.setRoles(Lists.newArrayList("ACCOUNT"));
        return new UsernamePasswordAuthenticationToken(user, null, authorityList);
    }
}