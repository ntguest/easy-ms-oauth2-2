package com.sgdbf.impulse.security.config.sso;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.security.oauth2.client.OAuth2SsoProperties;
import org.springframework.boot.autoconfigure.security.oauth2.resource.ResourceServerProperties;
import org.springframework.boot.autoconfigure.security.oauth2.resource.UserInfoTokenServices;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.filter.OAuth2ClientAuthenticationProcessingFilter;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeResourceDetails;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.inject.Inject;
import javax.servlet.Filter;

/**
 * @author yfo.
 */
@Configuration
@EnableOAuth2Client
@Order(2) // to be executed before ResourceServerConfiguration
@RequiredArgsConstructor
@EnableWebSecurity
public class SSOSecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final AuthorizationCodeResourceDetails details;
    private final ResourceServerProperties resourceServerProperties;
    private final SSOPrincipalExtractor principalExtractor;
    private final SSOSimpleUrlAuthenticationSuccessHandler authenticationSuccessHandler;
    private final SSOSimpleUrlAuthenticationFailureHandler authenticationFailureHandler;
    private final SSOLoginUrlAuthenticationEntryPoint authenticationEntryPoint;
    @Inject
    private OAuth2ClientContext oauth2ClientContext;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // @formatter:off
        http
                .requestMatchers().antMatchers("/oauth/authorize**", "/login**")
                .and()
                .authorizeRequests()
                .antMatchers("/login**")
                .permitAll()
                .anyRequest()
                .authenticated()
                .and()
                .exceptionHandling()
                .authenticationEntryPoint(authenticationEntryPoint)
                .and()
                .addFilterBefore(ssoFilter(), BasicAuthenticationFilter.class);
        // @formatter:on
    }

    private Filter ssoFilter() {
        OAuth2ClientAuthenticationProcessingFilter ssoFilter = new OAuth2ClientAuthenticationProcessingFilter(OAuth2SsoProperties.DEFAULT_LOGIN_PATH + "**");
        OAuth2RestTemplate restTemplate = new OAuth2RestTemplate(details, oauth2ClientContext);
        ssoFilter.setRestTemplate(restTemplate);
        UserInfoTokenServices tokenServices = new UserInfoTokenServices(resourceServerProperties.getUserInfoUri(), details.getClientId());
        tokenServices.setRestTemplate(restTemplate);
        tokenServices.setPrincipalExtractor(principalExtractor);
        ssoFilter.setTokenServices(tokenServices);
        ssoFilter.setAuthenticationSuccessHandler(authenticationSuccessHandler);
        ssoFilter.setAuthenticationFailureHandler(authenticationFailureHandler);
        return ssoFilter;
    }
}
