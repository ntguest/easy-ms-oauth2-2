package com.sgdbf.impulse.security.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.sgdbf.impulse.security.config.RealmContext;
import com.sgdbf.impulse.security.domain.OAuthUser;
import com.sgdbf.impulse.security.service.ImpulseUserDetails;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.security.jwt.crypto.sign.RsaSigner;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.stereotype.Component;

import java.security.KeyPair;
import java.security.interfaces.RSAPrivateKey;
import java.util.*;
import java.util.stream.Collectors;

import static com.sgdbf.impulse.security.utils.Constants.*;
import static org.apache.commons.lang.StringUtils.EMPTY;
import static org.springframework.security.oauth2.common.util.OAuth2Utils.CLIENT_ID;

/**
 * @author yfo.
 */
@Component
@RequiredArgsConstructor
public class TokenHelper {

    private final ObjectMapper objectMapper;
    private final KeyPair keyPair;
    private final PasswordEncoder passwordEncoder;

    public Map<String, Object> getAdditionalInformationWithIdToken(OAuth2AccessToken accessToken, ImpulseUserDetails user) {
        Map<String, Object> additionalInformation = accessToken.getAdditionalInformation();
        Map<String, Object> additionalInfo = Maps.newHashMap();
        additionalInfo.putAll(additionalInformation);
        additionalInfo.put(ID_TOKEN, generateIdToken(buildUserInformation(user)));
        return additionalInfo;
    }

    public Map<String, Object> buildUserInformation(ImpulseUserDetails impersonator) {
        Map<String, Object> map = Maps.newHashMap();
        map.put(SUB, impersonator.getUserId());
        map.put(ACCOUNT_ID, impersonator.getAccountId());
        map.put(LOGIN, impersonator.getLogin());
        map.put(FIRST_NAME, impersonator.getFirstName());
        map.put(LAST_NAME, impersonator.getLastName());
        map.put(ROLES, impersonator.getRoles());
        map.put(SITES, impersonator.getWebsites());
        map.put(IMPERSONATOR, impersonator.isImpersonator());
        map.put(SG_ID, impersonator.getSgId());
        return map;
    }

    public List<GrantedAuthority> createAuthorityList(List<String> roles, List<String> permissions) {
        List<GrantedAuthority> authorities = Optional.ofNullable(roles).orElse(Lists.newArrayList()).stream()
                .filter(StringUtils::isNotBlank)
                .map(r -> new SimpleGrantedAuthority("ROLE_" + r))
                .collect(Collectors.toList());

        authorities.addAll(Optional.ofNullable(permissions).orElse(Lists.newArrayList()).stream()
                .filter(StringUtils::isNotBlank)
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList()));

        return authorities;
    }

    public ImpulseUserDetails getImpulseUserDetailsPermissions(OAuthUser user, List<String> permissions) {
        List<GrantedAuthority> authorities = createAuthorityList(user.getRoles(), permissions);
        return buildImpulseUserDetails(user, authorities);
    }

    public ImpulseUserDetails getImpulseUserDetailsByAuthorities(OAuthUser user, List<GrantedAuthority> authorities) {
        return buildImpulseUserDetails(user, authorities);
    }

    public static OAuth2Request getOAuth2Request() {
        Map<String, String> requestParameters = ImmutableMap.of(CLIENT_ID, DRUPAL);
        Set<String> scopes = new HashSet<>();
        scopes.add(IMPULSE);
        return new OAuth2Request(requestParameters, DRUPAL, null, true, scopes, null, null, null, null);
    }

    public OAuthUser buildAdminATCOAuthUser(String sgId) {
        OAuthUser atc = new OAuthUser();
        atc.setId(sgId + AROBASE + RealmContext.getCurrentRealm());
        atc.setAccountId(sgId);
        atc.setPassword(EMPTY);
        atc.setEmailValidation(true);
        atc.setIsActive(true);
        atc.setRoles(Lists.newArrayList(ADMIN_ATC));
        atc.setWebSites(Lists.newArrayList(RealmContext.getCurrentRealm()));
        return atc;
    }

    public ImpulseUserDetails buildImpulseUserDetails(OAuthUser user, List<GrantedAuthority> authorities) {
        ImpulseUserDetails userDetails = new ImpulseUserDetails(user.getAccountId(), getHackedPassword(user.getPassword()), BooleanUtils.toBoolean(user.getIsActive()), user.getEmailValidation(), authorities);
        userDetails.setUserId(user.getId());
        userDetails.setAccountId(user.getAccountId());
        userDetails.setWebsites(user.getWebSites());
        userDetails.setRoles(user.getRoles());
        userDetails.setLogin(user.getLogin());
        userDetails.setFirstName(user.getFirstName());
        userDetails.setLastName(user.getLastName());
        return userDetails;
    }

    private String getHackedPassword(String password) {
        return Optional.ofNullable(password).filter(StringUtils::isNotBlank)
                // hack to avoid IllegalArgumentException when creating spring security User
                .orElse(passwordEncoder.encode(UUID.randomUUID().toString()));
    }

    private String generateIdToken(Map<String, Object> idTokenMap) {
        try {
            String content = objectMapper.writeValueAsString(idTokenMap);
            return JwtHelper.encode(content, getSigner()).getEncoded();
        } catch (JsonProcessingException e) {
            throw new IllegalStateException("Cannot format token id map to JSON", e);
        }
    }

    private RsaSigner getSigner() {
        return new RsaSigner((RSAPrivateKey) keyPair.getPrivate());
    }
}
