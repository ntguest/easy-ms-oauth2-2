package com.sgdbf.impulse.security.config;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.serializer.support.DeserializingConverter;
import org.springframework.core.serializer.support.SerializingConverter;
import org.springframework.session.data.mongo.JdkMongoSessionConverter;
import org.springframework.session.web.http.CookieSerializer;
import org.springframework.session.web.http.DefaultCookieSerializer;

import javax.annotation.PostConstruct;

/**
 * @author yfo.
 */
@Configuration
@RequiredArgsConstructor
public class SpringSessionMongoConfiguration {

    @Value("${spring.application.id}")
    private String oauth2ServiceId;

    /*
     * A NOTER :
     * - spring session mongo positionne un index TTL sur un champ expiredAt pour que les sessions se suppriment elle-même
     *   -> question : impact quelconque sur exploitation mongo ?
     */
    private final ApplicationContext context;

    // Important !!
    // Le converter JSON par défaut, ne sait pas comment gérer le lien OAuth2ClientContext(qui est en session)->AccessTokenRequest(qui est un bean scopé 'request')
    // cf https://github.com/spring-projects/spring-session-data-mongodb/issues/1
    @Bean
    public JdkMongoSessionConverter mongoSessionConverter() {
        // hack pour les devtools
        return new JdkMongoSessionConverter(new SerializingConverter(), new DeserializingConverter(this.getClass().getClassLoader()));
    }

    @PostConstruct
    public void checkApplicationContextId() {
        String id = context.getId();
        if (!id.contains(oauth2ServiceId)) {
            throw new IllegalStateException("ContextIdApplicationContextInitializer has determined a custom applicationId : '" + id + "'\n"
                    + "You should verify that this applicationId will be the same for ALL the instances of this application\n"
                    + "because it is crucial for the deserialiation of the session from Mongo on the differents instances");
            // see org.springframework.beans.factory.support.DefaultListableBeanFactory.setSerializationId(String)
        }
    }

    @Bean
    public CookieSerializer cookieSerializer() {
        DefaultCookieSerializer serializer = new DefaultCookieSerializer();
        serializer.setCookieName("IMPULSE_SESSIONID");
        // because its exposed via ZUUL
        serializer.setCookiePath("/" + oauth2ServiceId + "/");
        return serializer;
    }
}
