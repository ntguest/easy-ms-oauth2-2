package com.sgdbf.impulse.security.config.sso;

import com.sgdbf.impulse.common.domain.exception.ImpulseMessage;
import com.sgdbf.impulse.common.exception.MessagesFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * A custom authentication failure handler to delegate handling errors to redirectFailureUrl
 *
 * @author yfo.
 */
@Component
public class SSOSimpleUrlAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {

    @Value("${impulse.oauth2.authorization.failure.url}")
    private String redirectFailureUrl;

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        setDefaultFailureUrl(redirectFailureUrl + "?error=" + MessagesFactory.getOrDefault(exception.getMessage(), ImpulseMessage.access_denied).getKey());
        super.onAuthenticationFailure(request, response, new InternalAuthenticationServiceException(exception.getMessage()));
    }
}
