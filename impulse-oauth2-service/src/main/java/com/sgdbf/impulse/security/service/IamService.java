package com.sgdbf.impulse.security.service;

import com.google.common.collect.Lists;
import com.sgdbf.impulse.common.domain.exception.ImpulseMessage;
import com.sgdbf.impulse.common.ms.config.MultiWebsiteService;
import com.sgdbf.impulse.common.ms.feign.Try;
import com.sgdbf.impulse.common.website.config.WebsiteConfigService;
import com.sgdbf.impulse.iam.api.IamRestTemplateFactory;
import com.sgdbf.impulse.iam.domain.*;
import com.sgdbf.impulse.security.config.RealmContext;
import com.sgdbf.impulse.security.domain.OAuthUser;
import com.sgdbf.impulse.security.utils.Constants;
import com.sgdbf.impulse.security.utils.TokenHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.sgdbf.impulse.security.utils.Constants.*;

/**
 * @author yfo.
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class IamService {

    @Value("${iam.service.url}")
    private final String iamUrl;
    @Value("${iam.service.user}")
    private final String iamUser;
    @Value("${iam.service.password}")
    private final String iamPassword;

    private RestTemplate restTemplate;
    private final WebsiteConfigService websiteConfigService;
    private final TokenHelper tokenHelper;

    @PostConstruct
    public void init() {
        IamRestTemplateFactory factory = new IamRestTemplateFactory();
        restTemplate = factory.create(iamUser, iamPassword);
    }

    public OAuthUser getUserDetailBySgId(String sgId) {

        Optional<UserDetail> userDetail = Optional.ofNullable(sgId)
                .flatMap(this::getUserResult)
                .filter(u -> u.getPerson().getTotal() > 0)
                .map(this::extractIamId)
                .map(this::getUserDetails)
                .orElseThrow(() -> new InternalAuthenticationServiceException(ImpulseMessage.user_not_found.getKey()));

        return userDetail
                .filter(this::isAllowedToRealm)
                .map(u -> tokenHelper.buildAdminATCOAuthUser(sgId))
                // to be catched by AbstractAuthenticationProcessingFilter
                .orElseThrow(() -> new InternalAuthenticationServiceException(ImpulseMessage.access_denied.getKey()));
    }

    public Optional<SearchUserResult> getUserResult(String sgId) {
        return StringUtils.isBlank(sgId) ? Optional.empty() : Optional.ofNullable(getUserResultBySgId(sgId));
    }

    public Optional<UserDetail> getUserDetails(String iamId) {
        return StringUtils.isBlank(iamId) ? Optional.empty() : Optional.ofNullable(getUserDetailByIamId(iamId));
    }

    public Optional<RegionDetail> getRegion(String iamId) {
        return Optional.ofNullable(Try.of(() -> restTemplate.getForObject(iamUrl + IAM_REGION_VIEW_READ_URL + iamId, RegionDetail.class))
                .orElse(null));
    }

    private String extractIamId(SearchUserResult user) {
        return Optional.ofNullable(user.getPerson())
                .map(Contact::getRows)
                .orElse(Lists.newArrayList())
                .stream()
                .findFirst()
                .orElse(new Row())
                .getId();
    }

    private SearchUserResult getUserResultBySgId(String sgId) {
        return Try.of(() -> restTemplate.getForObject(iamUrl + IAM_PERSON_VIEW_SEARCH_URL + sgId, SearchUserResult.class)).orElse(null);
    }

    private UserDetail getUserDetailByIamId(String iamId) {
        return Try.of(() -> restTemplate.getForObject(iamUrl + IAM_PERSON_VIEW_READ_URL + iamId, UserDetail.class)).orElse(null);
    }

    private boolean isAllowedToRealm(UserDetail userDetails) {
        MultiWebsiteService.setCurrentWebsiteId(RealmContext.getCurrentRealm());
        Optional<RegionDetail> regionDetail = getRegion(userDetails.getRegion().getId());
        List<String> regionsCode = Arrays.asList(StringUtils.split(websiteConfigService.getConfiguration(Constants.IAM, Constants.CODE_REGION), ","));
        return regionDetail.map(region -> regionsCode.contains(region.getUid())).orElse(false);
    }
}
