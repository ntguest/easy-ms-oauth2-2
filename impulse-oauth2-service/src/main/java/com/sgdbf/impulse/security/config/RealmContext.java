package com.sgdbf.impulse.security.config;

/**
 * @author yfo.
 */
public class RealmContext {

    private static ThreadLocal<String> currentRealm = new ThreadLocal<>();

    public static void setCurrentRealm(String tenant) {
        currentRealm.set(tenant);
    }

    public static String getCurrentRealm() {
        return currentRealm.get();
    }

    public static void clear() {
        currentRealm.remove();
    }
}
