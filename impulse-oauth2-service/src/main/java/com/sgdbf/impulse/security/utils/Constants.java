package com.sgdbf.impulse.security.utils;

/**
 * @author yfo.
 */
public interface Constants {

    String ID_TOKEN = "id_token";
    String SUB = "sub";
    String ACCOUNT_ID = "accountId";
    String ROLES = "roles";
    String SITES = "sites";
    String LOGIN = "login";
    String FIRST_NAME = "firstName";
    String LAST_NAME = "lastName";
    String ID = "id";
    String WEBSITES = "webSites";
    String AROBASE = "@";
    String IMPERSONATOR = "impersonator";
    String SG_ID = "sgId";
    String DRUPAL = "drupal";
    String IMPULSE = "Impulse";
    String IAM = "iam";
    String CODE_REGION = "codeRegion";
    String ADMIN_ATC = "ADMIN_ATC";
    String IAM_PERSON_VIEW_SEARCH_URL = "/p/fr/PersonView/Search?query=0&Uid=";
    String IAM_PERSON_VIEW_READ_URL = "/p/fr/PersonView/Read/";
    String IAM_REGION_VIEW_READ_URL = "/p/fr/RegionView/Read/";
    String BECRYPTED_PASSWORD_PREFIX = "$2a$10";
}
