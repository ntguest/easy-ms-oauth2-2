package com.sgdbf.impulse.security.config;


import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.InputStreamResource;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.security.KeyPair;
import java.util.Comparator;
import java.util.Optional;

/**
 * @author yfo.
 */
@Configuration
public class KeyPairConfiguration {

    private static final char[] PASSWORD = "Impulse2018".toCharArray();
    private static final String ALIAS = "jwt";
    @Value("${impulse.keystore.path}")
    private String keystorePath;

    @Bean
    public KeyPair keyPair() {
        try {
            File secretsFolder = ResourceUtils.getFile(keystorePath);
            File[] jks = Optional.ofNullable(secretsFolder.listFiles((dir, name) -> name.endsWith(".jks") && name.startsWith("oauth2"))).orElse(new File[0]);
            File keystore = Lists.newArrayList(jks).stream().sorted(Comparator.comparing(File::getName).reversed()).findFirst().orElseThrow(() -> new FileNotFoundException("Keystore (JKS) not found"));
            InputStream inputStream = new FileInputStream(keystore);
            KeyStoreKeyFactory keyStoreKeyFactory = new KeyStoreKeyFactory(new InputStreamResource(inputStream), PASSWORD);
            return keyStoreKeyFactory.getKeyPair(ALIAS);
        } catch (FileNotFoundException e) {
            throw new IllegalStateException("Cannot found private key from keystore path " + keystorePath, e);
        }
    }
}
