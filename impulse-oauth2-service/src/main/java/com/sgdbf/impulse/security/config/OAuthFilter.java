package com.sgdbf.impulse.security.config;

import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author yfo.
 */
@Component
@Order(SecurityProperties.DEFAULT_FILTER_ORDER - 1000)
public class OAuthFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        try {
            RealmContext.setCurrentRealm(request.getParameter("realm"));
            filterChain.doFilter(request, response);
        } finally {
            RealmContext.clear();
        }
    }

    @Override
    public void destroy() {
    }
}
