package com.sgdbf.impulse.security;

import com.sgdbf.impulse.common.ms.mongo.InitialLoad;
import com.sgdbf.impulse.security.config.OAuthConfiguration;
import lombok.AllArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.system.ApplicationPidFileWriter;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Import;

/**
 * @author yfo.
 */
@SpringBootApplication
@Import(OAuthConfiguration.class)
@EnableEurekaClient
@AllArgsConstructor
public class OAuth2Application implements CommandLineRunner {

    private final InitialLoad initialLoad;

    public static void main(String[] args) {
        SpringApplication springApplication = new SpringApplication(OAuth2Application.class);
        springApplication.addListeners(new ApplicationPidFileWriter());
        springApplication.run(args);
    }

    @Override
    public void run(String... strings) {
        initialLoad.start();
    }
}
