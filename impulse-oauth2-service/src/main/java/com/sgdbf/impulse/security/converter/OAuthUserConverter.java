package com.sgdbf.impulse.security.converter;

import com.sgdbf.impulse.security.domain.OAuthUser;
import com.sgdbf.impulse.security.dto.OAuthUserDto;
import lombok.Data;

import java.util.function.Function;

/**
 * @author yfo.
 */
@Data(staticConstructor = "newInstance")
public class OAuthUserConverter implements Function<OAuthUser, OAuthUserDto> {

    @Override
    public OAuthUserDto apply(OAuthUser oAuthUser) {
        if (oAuthUser == null) {
            return null;
        }
        OAuthUserDto dto = new OAuthUserDto();
        dto.setId(oAuthUser.getId());
        dto.setAccountId(oAuthUser.getAccountId());
        dto.setLogin(oAuthUser.getLogin());
        dto.setEmailValidation(oAuthUser.getEmailValidation());
        dto.setRoles(oAuthUser.getRoles());
        dto.setWebSites(oAuthUser.getWebSites());
        return dto;
    }
}
