package com.sgdbf.impulse.security.config;

import com.sgdbf.impulse.security.service.ImpulseUserDetails;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.provider.token.DefaultUserAuthenticationConverter;

import java.util.Map;

import static com.sgdbf.impulse.security.utils.Constants.ACCOUNT_ID;
import static com.sgdbf.impulse.security.utils.Constants.WEBSITES;

/**
 * @author yfo.
 */
public class CustomUserAuthenticationConverter extends DefaultUserAuthenticationConverter {

    @Override
    public Map<String, ?> convertUserAuthentication(Authentication authentication) {
        Map<String, Object> response = (Map<String, Object>) super.convertUserAuthentication(authentication);
        ImpulseUserDetails principal = (ImpulseUserDetails) authentication.getPrincipal();
        response.put(WEBSITES, principal.getWebsites());
        response.put(ACCOUNT_ID, principal.getAccountId());
        return response;
    }
}
