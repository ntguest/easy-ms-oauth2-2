package com.sgdbf.impulse.security.service;

import com.sgdbf.impulse.common.domain.exception.ImpulseMessage;
import com.sgdbf.impulse.security.config.RealmContext;
import com.sgdbf.impulse.security.domain.OAuthUser;
import com.sgdbf.impulse.security.utils.TokenHelper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Optional;

/**
 * @author yfo.
 */
@Slf4j
@Service
@AllArgsConstructor
public class ImpulseUserDetailsService implements UserDetailsService {

    private final OAuthUserService oAuthUserService;
    private final IamService iamService;
    private final TokenHelper tokenHelper;
    private static final String SG_ID_REGEX = "[a-zA-Z]\\d{7}";

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        String realm = RealmContext.getCurrentRealm();
        Optional<OAuthUser> found = oAuthUserService.findOAuthUserByIdOrLoginAndWebsitesId(login, Collections.singletonList(realm));
        boolean match = com.sgdbf.impulse.common.utils.StringUtils.match(login, SG_ID_REGEX);
        if (!found.isPresent() && match) {
            found = Optional.ofNullable(iamService.getUserDetailBySgId(login));
        }
        if (!found.isPresent()) {
            log.info("User not found for login {} and realm {}", login, realm);
        }
        OAuthUser user = found.orElseThrow(() -> new UsernameNotFoundException(ImpulseMessage.bad_credentials.getKey()));
        log.info("User loaded by {}, {}, {}", user.getId(), user.getLogin(), user.getRoles());
        return tokenHelper.getImpulseUserDetailsPermissions(user, oAuthUserService.findPermissionsByRoles(user.getRoles()));
    }
}