#!/bin/sh

set -x -e

if [ -d "/run/secrets" ]; then
  for filepath in /run/secrets/*.cer; do
    filename="$(basename "$filepath")"
    echo "import certificat" ${filename} "to $JAVA_HOME/jre/lib/security/cacerts"
    keytool -noprompt -keystore $JAVA_HOME/jre/lib/security/cacerts -importcert -alias ${filename} -file ${filepath} -storepass changeit
  done
fi